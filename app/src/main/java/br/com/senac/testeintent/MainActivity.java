package br.com.senac.testeintent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity {


    public static String NUMERO = "numero";
    public static String ALUNO = "aluno";

    private EditText txtNome;
    private EditText txtSobreNome;
    private Button btnSalvar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNome = findViewById(R.id.txtNome);
        txtSobreNome = findViewById(R.id.txtSobreNome);
        btnSalvar = findViewById(R.id.btnSalvar);


        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String nome = txtNome.getText().toString();
                String sobrenome = txtSobreNome.getText().toString();

                Aluno aluno = new Aluno(nome, sobrenome);

                Intent intent = new Intent(MainActivity.this, ResultadoActivity.class);
                intent.putExtra(MainActivity.NUMERO, 10);
                intent.putExtra(MainActivity.ALUNO, aluno);
                startActivity(intent);
            }
        });


    }
}
