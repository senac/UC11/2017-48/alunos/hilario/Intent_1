package br.com.senac.testeintent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class ResultadoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        Intent intent = getIntent();

        int a = intent.getIntExtra(MainActivity.NUMERO, 0);
        Aluno aluno = (Aluno)intent.getSerializableExtra(MainActivity.ALUNO);

        intent.getSerializableExtra(MainActivity.ALUNO);

        Toast.makeText(this, "numero: " + a, Toast.LENGTH_SHORT).show();
        Toast.makeText(this, "Aluno: " + aluno.getNome() + " " + aluno.getSobrenome(), Toast.LENGTH_SHORT).show();

    }
}
