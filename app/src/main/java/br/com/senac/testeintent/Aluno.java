package br.com.senac.testeintent;

import java.io.Serializable;

/**
 * Created by sala302b on 31/01/2018.
 */

public class Aluno implements Serializable {

    private String nome;
    private String sobrenome;

    public Aluno(){

    }

    public Aluno(String nome, String sobrenome){
        this.nome = nome;
        this.sobrenome = sobrenome;

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }
}
